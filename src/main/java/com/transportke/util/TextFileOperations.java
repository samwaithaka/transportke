package com.transportke.util;

import java.io.FileWriter;
import java.io.IOException;

public class TextFileOperations {
    public static boolean writePageUrl(String filePath, String text, boolean append) {
    	try
    	{
    	    FileWriter fw = new FileWriter(filePath,append);
    	    String url = "";
    	    String ln = "";
    	    if(append == true) {
    	    	url = Configs.getConfig("appurl");
    	    	ln = "\n";
    	    }
    	    fw.write(url + text + ln);
    	    fw.close();
    	    return true;
    	} catch(IOException ioe) {
    	    System.err.println("IOException: " + ioe.getMessage());
    	    return false;
    	}
    }
}

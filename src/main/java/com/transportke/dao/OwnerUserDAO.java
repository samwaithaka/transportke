package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.OwnerUser;

public class OwnerUserDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static OwnerUser addOwnerUser(OwnerUser ownerUser) {
    	OwnerUser ownerUser2 = findExistingOwnerUser(ownerUser.getEmailAddress());
    	if(ownerUser2.getId() == 0) {
	    	ownerUser.setCreatedOn(new Timestamp(System.currentTimeMillis()));
	    	ownerUser.setEditedOn(new Timestamp(System.currentTimeMillis()));
	    	ownerUser.setCreatedBy(ownerUser.getEditedBy());
	    	em = factory.createEntityManager();
	        em.getTransaction().begin();
	        em.persist(ownerUser);
	        em.getTransaction().commit();
	        em.close();
    	} else {
    		ownerUser = ownerUser2;
    	}
        return ownerUser;
    }
    
    public static OwnerUser findExistingOwnerUser(String idNumber) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select o from OwnerUser o WHERE o.idNumber = :idNumber");
        q.setParameter("idNumber", idNumber);
        OwnerUser ownerUser = new OwnerUser();
    	try {
    	    ownerUser = (OwnerUser) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return ownerUser;
    }
    
    public static OwnerUser login(String idNumber, String password) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select o from OwnerUser o WHERE o.idNumber LIKE :idNumber AND o.password LIKE :password");
    	q.setParameter("idNumber", idNumber);
    	q.setParameter("password", password);
        OwnerUser ownerUser = new OwnerUser();
    	try {
    	    ownerUser = (OwnerUser) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return ownerUser;
    }

    
    public static OwnerUser updateOwnerUser(OwnerUser ownerUser) {
    	ownerUser.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(ownerUser);
        em.getTransaction().commit();
        em.close();
        return ownerUser;
    }
    
    public static OwnerUser find(int id) {
     	em = factory.createEntityManager();
     	OwnerUser ownerUser = em.find(OwnerUser.class, id);
     	em.close();
     	return ownerUser;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<OwnerUser> getOwnerUserList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM OwnerUser u WHERE u.active=true");
     	List<OwnerUser> ownerUserList = new ArrayList<OwnerUser>();
     	try {
     	    ownerUserList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return ownerUserList;
     }
}

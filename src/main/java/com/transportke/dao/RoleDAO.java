package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.Role;

public class RoleDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static Role addRole(Role role) {
    	role.setCreatedOn(new Timestamp(System.currentTimeMillis()));
    	role.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	role.setCreatedBy(role.getEditedBy());
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(role);
        em.getTransaction().commit();
        em.close();
        return role;
    }
    
    public static Role updateRole(Role role) {
    	role.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(role);
        em.getTransaction().commit();
        em.close();
        return role;
    }
    
    public static Role find(int id) {
     	em = factory.createEntityManager();
     	Role role = em.find(Role.class, id);
     	em.close();
     	return role;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<Role> getRoleList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM Role u WHERE u.active=true");
     	List<Role> roleList = new ArrayList<Role>();
     	try {
     	    roleList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return roleList;
     }
}

package com.transportke.dao;

import java.sql.Timestamp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.Staff;
import com.transportke.models.WorkLog;

public class WorkLogDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static WorkLog openWorkLog(Staff staff) {
    	WorkLog workLog = new WorkLog();
    	workLog.setStaff(staff);
    	workLog.setStartAt(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(workLog);
        em.getTransaction().commit();
        em.close();
        return workLog;
    }
    
    public static WorkLog closeWorkLog(WorkLog workLog) {
    	workLog.setEndAt(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(workLog);
        em.getTransaction().commit();
        em.close();
        return workLog;
    }
    
    public static WorkLog find(int id) {
     	em = factory.createEntityManager();
     	WorkLog workLog = em.find(WorkLog.class, id);
     	em.close();
     	return workLog;
    }
    
    public static WorkLog findOpenWorkLog(Staff staff) {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM WorkLog u WHERE u.staff=:staff AND u.endAt IS NULL");
     	q.setParameter("staff", staff);
     	WorkLog workLog = new WorkLog();
     	try {
     	    workLog = (WorkLog) q.getSingleResult();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	em.close();
     	return workLog;
    }
}

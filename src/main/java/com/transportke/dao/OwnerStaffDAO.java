package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.OwnerStaff;

public class OwnerStaffDAO {

	private static final String PERSISTENCE_UNIT_NAME = "transportke";
	private static EntityManager em;
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

	public static OwnerStaff addOwnerStaff(OwnerStaff ownerStaff) {
		ownerStaff.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		ownerStaff.setEditedOn(new Timestamp(System.currentTimeMillis()));
		ownerStaff.setCreatedBy(ownerStaff.getEditedBy());
		em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(ownerStaff);
		em.getTransaction().commit();
		em.close();
		return ownerStaff;
	}

	public static OwnerStaff updateOwnerStaff(OwnerStaff ownerStaff) {
		ownerStaff.setEditedOn(new Timestamp(System.currentTimeMillis()));
		em = factory.createEntityManager();
		em.getTransaction().begin();
		em.merge(ownerStaff);
		em.getTransaction().commit();
		em.close();
		return ownerStaff;
	}

	public static OwnerStaff find(int id) {
		em = factory.createEntityManager();
		OwnerStaff ownerStaff = em.find(OwnerStaff.class, id);
		em.close();
		return ownerStaff;
	}

	@SuppressWarnings("unchecked")
	public static List<OwnerStaff> getOwnerStaffList() {
		em = factory.createEntityManager();
		Query q = em.createQuery("SELECT u FROM OwnerStaff u WHERE u.active=true");
		List<OwnerStaff> ownerStaffList = new ArrayList<OwnerStaff>();
		try {
			ownerStaffList = q.getResultList();
		} catch(NoResultException e) {
			System.out.println("No Results Exception");
		}
		return ownerStaffList;
	}
}

package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.OwnerVehicle;

public class OwnerVehicleDAO {

	private static final String PERSISTENCE_UNIT_NAME = "transportke";
	private static EntityManager em;
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

	public static OwnerVehicle addOwnerVehicle(OwnerVehicle ownerVehicle) {
		ownerVehicle.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		ownerVehicle.setEditedOn(new Timestamp(System.currentTimeMillis()));
		ownerVehicle.setCreatedBy(ownerVehicle.getEditedBy());
		em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(ownerVehicle);
		em.getTransaction().commit();
		em.close();
		return ownerVehicle;
	}

	public static OwnerVehicle updateOwnerVehicle(OwnerVehicle ownerVehicle) {
		ownerVehicle.setEditedOn(new Timestamp(System.currentTimeMillis()));
		em = factory.createEntityManager();
		em.getTransaction().begin();
		em.merge(ownerVehicle);
		em.getTransaction().commit();
		em.close();
		return ownerVehicle;
	}

	public static OwnerVehicle find(int id) {
		em = factory.createEntityManager();
		OwnerVehicle ownerVehicle = em.find(OwnerVehicle.class, id);
		em.close();
		return ownerVehicle;
	}

	@SuppressWarnings("unchecked")
	public static List<OwnerVehicle> getOwnerVehicleList() {
		em = factory.createEntityManager();
		Query q = em.createQuery("SELECT u FROM OwnerVehicle u WHERE u.active=true");
		List<OwnerVehicle> ownerVehicleList = new ArrayList<OwnerVehicle>();
		try {
			ownerVehicleList = q.getResultList();
		} catch(NoResultException e) {
			System.out.println("No Results Exception");
		}
		return ownerVehicleList;
	}
}

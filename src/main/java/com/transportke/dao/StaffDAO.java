package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.OwnerUser;
import com.transportke.models.Staff;

public class StaffDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static Staff addStaff(Staff staff) {
    	Staff staff2 = findExistingStaff(staff.getIdNumber());
    	if(staff2.getId() == 0) {
	    	staff.setCreatedOn(new Timestamp(System.currentTimeMillis()));
	    	staff.setEditedOn(new Timestamp(System.currentTimeMillis()));
	    	staff.setCreatedBy(staff.getEditedBy());
	    	em = factory.createEntityManager();
	        em.getTransaction().begin();
	        em.persist(staff);
	        em.getTransaction().commit();
	        em.close();
    	} else {
    		staff = staff2;
    	}
        return staff;
    }
    
    public static Staff findExistingStaff(String idNumber) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select m from Staff m WHERE m.idNumber = :idNumber");
        q.setParameter("idNumber", idNumber);
        Staff staff = new Staff();
    	try {
    	    staff = (Staff) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return staff;
    }
    
    public static Staff updateStaff(Staff staff) {
    	staff.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(staff);
        em.getTransaction().commit();
        em.close();
        return staff;
    }
    
    public static Staff find(int id) {
     	em = factory.createEntityManager();
     	Staff staff = em.find(Staff.class, id);
     	em.close();
     	return staff;
    }
    
    public static Staff login(String idNumber, String password) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select o from Staff o WHERE o.idNumber LIKE :idNumber AND o.password LIKE :password");
    	q.setParameter("idNumber", idNumber);
    	q.setParameter("password", password);
    	Staff staff = new Staff();
    	try {
    		staff = (Staff) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return staff;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<Staff> getOwnerStaffList(OwnerUser ownerUser) {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM Staff u WHERE u.ownerUser=:ownerUser AND u.active=true");
     	q.setParameter("ownerUser", ownerUser);
     	List<Staff> staffList = new ArrayList<Staff>();
     	try {
     	    staffList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return staffList;
     }
    
    @SuppressWarnings("unchecked")
 	public static List<Staff> getStaffList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM Staff u WHERE u.active=true");
     	List<Staff> staffList = new ArrayList<Staff>();
     	try {
     	    staffList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return staffList;
     }
}

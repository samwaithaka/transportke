package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.Route;

public class RouteDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static Route addRoute(Route route) {
    	route.setCreatedOn(new Timestamp(System.currentTimeMillis()));
    	route.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	route.setCreatedBy(route.getEditedBy());
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(route);
        em.getTransaction().commit();
        em.close();
        return route;
    }
    
    public static Route updateRoute(Route route) {
    	route.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(route);
        em.getTransaction().commit();
        em.close();
        return route;
    }
    
    public static Route find(int id) {
     	em = factory.createEntityManager();
     	Route route = em.find(Route.class, id);
     	em.close();
     	return route;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<Route> getRouteList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM Route u WHERE u.active=true");
     	List<Route> routeList = new ArrayList<Route>();
     	try {
     	    routeList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return routeList;
     }
}

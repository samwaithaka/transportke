package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.MotorVehicle;
import com.transportke.models.OwnerUser;
import com.transportke.models.Staff;

public class MotorVehicleDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static MotorVehicle addMotorVehicle(MotorVehicle motorVehicle) {
    	MotorVehicle motorVehicle2 = findExistingMotorVehicle(motorVehicle.getRegistrationNumber());
    	if(motorVehicle2.getId() == 0) {
	    	motorVehicle.setCreatedOn(new Timestamp(System.currentTimeMillis()));
	    	motorVehicle.setEditedOn(new Timestamp(System.currentTimeMillis()));
	    	motorVehicle.setCreatedBy(motorVehicle.getEditedBy());
	    	em = factory.createEntityManager();
	        em.getTransaction().begin();
	        em.persist(motorVehicle);
	        em.getTransaction().commit();
	        em.close();
    	} else {
    		motorVehicle = motorVehicle2;
    	}
        return motorVehicle;
    }
    
    private static MotorVehicle findExistingMotorVehicle(String registrationNumber) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select m from MotorVehicle m WHERE m.registrationNumber = :registrationNumber");
        q.setParameter("registrationNumber", registrationNumber);
        MotorVehicle motorVehicle = new MotorVehicle();
    	try {
    	    motorVehicle = (MotorVehicle) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return motorVehicle;
    }
    
    public static MotorVehicle updateMotorVehicle(MotorVehicle motorVehicle) {
    	motorVehicle.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(motorVehicle);
        em.getTransaction().commit();
        em.close();
        return motorVehicle;
    }
    
    public static MotorVehicle find(int id) {
     	em = factory.createEntityManager();
     	MotorVehicle motorVehicle = em.find(MotorVehicle.class, id);
     	em.close();
     	return motorVehicle;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<MotorVehicle> getOwnerMotorVehicleList(OwnerUser ownerUser) {
    	System.out.println("MotorVehicle for " + ownerUser);
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM MotorVehicle u WHERE u.ownerUser=:ownerUser AND u.active=true");
     	q.setParameter("ownerUser", ownerUser);
     	List<MotorVehicle> motorVehicleList = new ArrayList<MotorVehicle>();
     	try {
     		motorVehicleList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return motorVehicleList;
     }

    @SuppressWarnings("unchecked")
 	public static List<MotorVehicle> getMotorVehicleList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM MotorVehicle u WHERE u.active=true");
     	List<MotorVehicle> motorVehicleList = new ArrayList<MotorVehicle>();
     	try {
     	    motorVehicleList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return motorVehicleList;
     }
    @SuppressWarnings("unchecked")
 	public static List<MotorVehicle> getOwnerMotorVehicle(OwnerUser ownerUser) {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM MotorVehicle u WHERE u.ownerUser=:ownerUser AND u.active=true");
     	q.setParameter("ownerUser", ownerUser);
     	List<MotorVehicle> motorVehicleList = new ArrayList<MotorVehicle>();
     	try {
     		motorVehicleList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return motorVehicleList;
     }
    
 	public static MotorVehicle getStaffMotorVehicle(Staff staff) {
     	em = factory.createEntityManager();
     	Query q = em.createNativeQuery("SELECT * FROM motor_vehicle WHERE conductor_id=?staffId OR driver_id=?staffId ORDER BY id desc LIMIT 1", MotorVehicle.class);
     	q.setParameter("staffId", staff.getId());
     	MotorVehicle motorVehicle = new MotorVehicle();
     	try {
     		motorVehicle = (MotorVehicle) q.getSingleResult();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return motorVehicle;
     }
}

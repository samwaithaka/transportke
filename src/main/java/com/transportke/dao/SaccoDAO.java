package com.transportke.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.transportke.models.Sacco;

public class SaccoDAO {
	
	private static final String PERSISTENCE_UNIT_NAME = "transportke";
    private static EntityManager em;
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    public static Sacco addSacco(Sacco sacco) {
    	Sacco sacco2 = findExistingSacco(sacco.getRegistrationNumber());
    	if(sacco2.getId() == 0) {
	    	sacco.setCreatedOn(new Timestamp(System.currentTimeMillis()));
	    	sacco.setEditedOn(new Timestamp(System.currentTimeMillis()));
	    	sacco.setCreatedBy(sacco.getEditedBy());
	    	em = factory.createEntityManager();
	        em.getTransaction().begin();
	        em.persist(sacco);
	        em.getTransaction().commit();
	        em.close();
    	} else {
    		sacco = sacco2;
    	}
        return sacco;
    }
    
    private static Sacco findExistingSacco(String registrationNumber) {
    	em = factory.createEntityManager();
    	Query q = em.createQuery("select m from Sacco m WHERE m.registrationNumber = :registrationNumber");
        q.setParameter("registrationNumber", registrationNumber);
        Sacco sacco = new Sacco();
    	try {
    	    sacco = (Sacco) q.getSingleResult();
    	} catch(NoResultException e) {
    		System.out.println("No Results Exception");
    	}
    	return sacco;
    }
    
    public static Sacco updateSacco(Sacco sacco) {
    	sacco.setEditedOn(new Timestamp(System.currentTimeMillis()));
    	em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(sacco);
        em.getTransaction().commit();
        em.close();
        return sacco;
    }
    
    public static Sacco find(int id) {
     	em = factory.createEntityManager();
     	Sacco sacco = em.find(Sacco.class, id);
     	em.close();
     	return sacco;
    }
    
    @SuppressWarnings("unchecked")
 	public static List<Sacco> getSaccoList() {
     	em = factory.createEntityManager();
     	Query q = em.createQuery("SELECT u FROM Sacco u WHERE u.active=true");
     	List<Sacco> saccoList = new ArrayList<Sacco>();
     	try {
     	    saccoList = q.getResultList();
     	} catch(NoResultException e) {
     		System.out.println("No Results Exception");
     	}
     	return saccoList;
     }
}

package com.transportke.models;

import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "motor_vehicle")
public class MotorVehicle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "registration_number", length=16)
	private String registrationNumber;
	@Column(name = "vehicle_make", length=64)
	private String vehicleMake;
	@Column(name = "vehicle_model", length=64)
	private String vehicleModel;
	@Column(name = "vehicle_color", length=32)
	private String vehicleColor;
	@Column(name = "manufacture_year")
	private int manufactureYear;
	@Column(name = "carrying_capacity")
	private int carryingCapacity;
	@Column(name = "insurance_cert_number", length=32)
	private String insuranceCertNumber;
	@Column(name = "insurance_expiry_date")
	private Date insuranceExpiryDate;
	@Column(name = "last_ntsa_inspection_date")
	private Date lastNTSAInspectionDate;
	@Column(name = "last_ntsa_inspection_center", length=64)
	private String lastNTSAInspectionCenter;
	@Column(name = "speed_governor_model", length=64)
	private String speedGovernorModel;
	@Column(name = "speed_governor_serial_no", length=64)
	private String speedGovernorSerialNo;
	@Column(name = "speed_governor_annual_cert_no", length=64)
	private String speedGovernorAnnualCertNo;
	@Column(name = "speed_governor_annual_cert_expiry")
	private Date speedGovernorAnnualCertExpiry;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_on")
	private Timestamp createdOn;
	@Column(name = "created_by", length=64)
	private String createdBy;
	@Column(name = "edited_on")
	private Timestamp editedOn;
	@Column(name = "edited_by", length=64)
	private String editedBy;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="driver_id")
    private Staff driver = new Staff();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="conductor_id")
    private Staff conductor = new Staff();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sacco_id")
    private Sacco sacco = new Sacco();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
    private OwnerUser ownerUser = new OwnerUser();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getVehicleMake() {
		return vehicleMake;
	}
	public void setVehicleMake(String vehicleMake) {
		this.vehicleMake = vehicleMake;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleColor() {
		return vehicleColor;
	}
	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}
	public int getManufactureYear() {
		return manufactureYear;
	}
	public void setManufactureYear(int manufactureYear) {
		this.manufactureYear = manufactureYear;
	}
	public int getCarryingCapacity() {
		return carryingCapacity;
	}
	public void setCarryingCapacity(int carryingCapacity) {
		this.carryingCapacity = carryingCapacity;
	}
	public String getInsuranceCertNumber() {
		return insuranceCertNumber;
	}
	public void setInsuranceCertNumber(String insuranceCertNumber) {
		this.insuranceCertNumber = insuranceCertNumber;
	}
	public Date getInsuranceExpiryDate() {
		return insuranceExpiryDate;
	}
	public void setInsuranceExpiryDate(Date insuranceExpiryDate) {
		this.insuranceExpiryDate = insuranceExpiryDate;
	}
	public Date getLastNTSAInspectionDate() {
		return lastNTSAInspectionDate;
	}
	public void setLastNTSAInspectionDate(Date lastNTSAInspectionDate) {
		this.lastNTSAInspectionDate = lastNTSAInspectionDate;
	}
	public String getLastNTSAInspectionCenter() {
		return lastNTSAInspectionCenter;
	}
	public void setLastNTSAInspectionCenter(String lastNTSAInspectionCenter) {
		this.lastNTSAInspectionCenter = lastNTSAInspectionCenter;
	}
	public String getSpeedGovernorModel() {
		return speedGovernorModel;
	}
	public void setSpeedGovernorModel(String speedGovernorModel) {
		this.speedGovernorModel = speedGovernorModel;
	}
	public String getSpeedGovernorSerialNo() {
		return speedGovernorSerialNo;
	}
	public void setSpeedGovernorSerialNo(String speedGovernorSerialNo) {
		this.speedGovernorSerialNo = speedGovernorSerialNo;
	}
	public String getSpeedGovernorAnnualCertNo() {
		return speedGovernorAnnualCertNo;
	}
	public void setSpeedGovernorAnnualCertNo(String speedGovernorAnnualCertNo) {
		this.speedGovernorAnnualCertNo = speedGovernorAnnualCertNo;
	}
	public Date getSpeedGovernorAnnualCertExpiry() {
		return speedGovernorAnnualCertExpiry;
	}
	public void setSpeedGovernorAnnualCertExpiry(Date speedGovernorAnnualCertExpiry) {
		this.speedGovernorAnnualCertExpiry = speedGovernorAnnualCertExpiry;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Staff getDriver() {
		return driver;
	}
	public void setDriver(Staff driver) {
		this.driver = driver;
	}
	public Staff getConductor() {
		return conductor;
	}
	public void setConductor(Staff conductor) {
		this.conductor = conductor;
	}
	public Sacco getSacco() {
		return sacco;
	}
	public void setSacco(Sacco sacco) {
		this.sacco = sacco;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp  createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public OwnerUser getOwnerUser() {
		return ownerUser;
	}
	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}
	public Timestamp getEditedOn() {
		return editedOn;
	}
	public void setEditedOn(Timestamp editedOn) {
		this.editedOn = editedOn;
	}
	public String getEditedBy() {
		return editedBy;
	}
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}
	@Override
	public String toString() {
		return "MotorVehicle [id=" + id + ", registrationNumber=" + registrationNumber + ", vehicleMake=" + vehicleMake
				+ ", vehicleModel=" + vehicleModel + ", vehicleColor=" + vehicleColor + ", manufactureYear="
				+ manufactureYear + ", carryingCapacity=" + carryingCapacity + ", insuranceCertNumber="
				+ insuranceCertNumber + ", insuranceExpiryDate=" + insuranceExpiryDate + ", lastNTSAInspectionDate="
				+ lastNTSAInspectionDate + ", lastNTSAInspectionCenter=" + lastNTSAInspectionCenter
				+ ", speedGovernorModel=" + speedGovernorModel + ", speedGovernorSerialNo=" + speedGovernorSerialNo
				+ ", speedGovernorAnnualCertNo=" + speedGovernorAnnualCertNo + ", speedGovernorAnnualCertExpiry="
				+ speedGovernorAnnualCertExpiry + ", active=" + active + ", driver=" + driver + ", conductor="
				+ conductor + "]";
	}
}

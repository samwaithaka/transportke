package com.transportke.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "work_log")
public class WorkLog {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="staff_id")
    private Staff staff;
	@Column(name = "start_at")
	private Timestamp startAt;
	@Column(name = "end_at")
	private Timestamp endAt;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Timestamp getStartAt() {
		return startAt;
	}
	
	public void setStartAt(Timestamp startAt) {
		this.startAt = startAt;
	}
	
	public Timestamp getEndAt() {
		return endAt;
	}
	
	public void setEndAt(Timestamp endAt) {
		this.endAt = endAt;
	}
	
	public Staff getStaff() {
		return staff;
	}
	
	public void setStaff(Staff staff) {
		this.staff = staff;
	}
}

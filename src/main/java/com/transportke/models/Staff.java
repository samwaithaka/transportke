package com.transportke.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class Staff {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "id_number", length=16)
	private String idNumber;
	@Column(name = "password", length=64)
	private String password;
	@Column(name = "staff_name", length=64)
	private String staffName;
	@Column(name = "postal_address", length=128)
	private String postalAddress;
	@Column(name = "date_of_birth")
	private String dateOfBirth;
	@Column(name = "phone_number", length=32)
	private String phoneNumber;
	@Column(name = "email_address", length=128)
	private String emailAddress;
	@Column(name = "id_file_name", length=32)
	private String idFileName;
	@Column(name = "psv_file_name", length=32)
	private String psvFileName;
	@Column(name = "active")
	private boolean active = true;
	@Column(name = "created_on")
	private Timestamp createdOn;
	@Column(name = "created_by", length=64)
	private String createdBy;
	@Column(name = "edited_on")
	private Timestamp editedOn;
	@Column(name = "edited_by", length=64)
	private String editedBy;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="role_id")
    private Role role = new Role();
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
    private OwnerUser ownerUser = new OwnerUser();
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getIdFileName() {
		return idFileName;
	}
	public void setIdFileName(String idFileName) {
		this.idFileName = idFileName;
	}
	public String getPsvFileName() {
		return psvFileName;
	}
	public void setPsvFileName(String psvFileName) {
		this.psvFileName = psvFileName;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getEditedOn() {
		return editedOn;
	}
	public void setEditedOn(Timestamp editedOn) {
		this.editedOn = editedOn;
	}
	public String getEditedBy() {
		return editedBy;
	}
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}
	public OwnerUser getOwnerUser() {
		return ownerUser;
	}
	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Staff [id=" + id + ", idNumber=" + idNumber + ", staffName=" + staffName + ", postalAddress="
				+ postalAddress + ", dateOfBirth=" + dateOfBirth + ", phoneNumber=" + phoneNumber + ", emailAddress="
				+ emailAddress + ", idFileName=" + idFileName + ", psvFileName=" + psvFileName + ", active=" + active
				+ ", role=" + role + "]";
	}
}

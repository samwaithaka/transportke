package com.transportke.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class OwnerVehicle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "active")
	private boolean active;
	@Column(name = "created_on")
	private Timestamp createdOn;
	@Column(name = "created_by", length=64)
	private String createdBy;
	@Column(name = "edited_on")
	private Timestamp editedOn;
	@Column(name = "edited_by", length=64)
	private String editedBy;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
    private OwnerUser ownerUser;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="vehicle_id")
    private MotorVehicle motorVehicle;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public OwnerUser getOwnerUser() {
		return ownerUser;
	}
	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}
	public MotorVehicle getMotorVehicle() {
		return motorVehicle;
	}
	public void setMotorVehicle(MotorVehicle motorVehicle) {
		this.motorVehicle = motorVehicle;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getEditedOn() {
		return editedOn;
	}
	public void setEditedOn(Timestamp editedOn) {
		this.editedOn = editedOn;
	}
	public String getEditedBy() {
		return editedBy;
	}
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}
	@Override
	public String toString() {
		return "OwnerVehicle [id=" + id + ", active=" + active + ", ownerUser=" + ownerUser + ", motorVehicle="
				+ motorVehicle + "]";
	}
}

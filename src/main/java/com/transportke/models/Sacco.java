package com.transportke.models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sacco")
public class Sacco {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "registration_number", length=32)
	private String registrationNumber;
	@Column(name = "sacco_name", length=64)
	private String saccoName;
	@Column(name = "postal_address", length=512)
	private String postalAddress;
	@Column(name = "email_address", length=512)
	private String emailAddress;
	@Column(name = "active")
	private boolean active = true;
	@Column(name = "created_on")
	private Timestamp createdOn;
	@Column(name = "created_by", length=64)
	private String createdBy;
	@Column(name = "edited_on")
	private Timestamp editedOn;
	@Column(name = "edited_by", length=64)
	private String editedBy;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="route_id")
    private Route route = new Route();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getSaccoName() {
		return saccoName;
	}
	public void setSaccoName(String saccoName) {
		this.saccoName = saccoName;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getEditedOn() {
		return editedOn;
	}
	public void setEditedOn(Timestamp editedOn) {
		this.editedOn = editedOn;
	}
	public String getEditedBy() {
		return editedBy;
	}
	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}
	@Override
	public String toString() {
		return "Sacco [id=" + id + ", registrationNumber=" + registrationNumber + ", saccoName=" + saccoName
				+ ", postalAddress=" + postalAddress + ", emailAddress=" + emailAddress + ", active=" + active
				+ ", route=" + route + "]";
	}
}
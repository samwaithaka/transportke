package com.transportke.controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.transportke.dao.MotorVehicleDAO;
import com.transportke.dao.RoleDAO;
import com.transportke.dao.StaffDAO;
import com.transportke.models.MotorVehicle;
import com.transportke.models.OwnerUser;
import com.transportke.models.Staff;

@ManagedBean(name="staffController", eager=true)
@SessionScoped
public class StaffController {
	
	private Staff staff;
	private OwnerUser ownerUser;
	private MotorVehicle motorVehicle;
	private List<Staff> staffList;
	
	@PostConstruct
	public void init() {
		staff = new Staff();
	}
	
	public StaffController() {
	}
	
	public void refresh() {
		staffList = StaffDAO.getOwnerStaffList(ownerUser);
		System.out.println("Here: " + ownerUser + staffList);
	}
	
	public String addStaff() {
		staff.setRole(RoleDAO.find(staff.getRole().getId()));
		staff.setCreatedBy("Self");
		staff.setEditedBy("Self");
		staff.setOwnerUser(ownerUser);
		StaffDAO.addStaff(staff);
		staffList = StaffDAO.getOwnerStaffList(ownerUser);
		staff = new Staff();
	    return "admin-list-staff.xhtml?faces-redirect=true";
	}
	
	public String updateStaff() {
		staff.setRole(RoleDAO.find(staff.getRole().getId()));
	    StaffDAO.updateStaff(staff);
	    staffList = StaffDAO.getOwnerStaffList(ownerUser);
	    staff = new Staff();
	    return "admin-list-staff.xhtml?faces-redirect=true";
	}
	
	public String login() {
		staff = StaffDAO.login(staff.getIdNumber(), staff.getPassword());
		motorVehicle = MotorVehicleDAO.getStaffMotorVehicle(staff);
		return "staff.xhtml?faces-redirect=true";
	}
	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public List<Staff> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<Staff> staffList) {
		this.staffList = staffList;
	}

	public OwnerUser getOwnerUser() {
		return ownerUser;
	}

	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}

	public MotorVehicle getMotorVehicle() {
		return motorVehicle;
	}

	public void setMotorVehicle(MotorVehicle motorVehicle) {
		this.motorVehicle = motorVehicle;
	}
}
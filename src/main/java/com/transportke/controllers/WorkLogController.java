package com.transportke.controllers;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.transportke.dao.WorkLogDAO;
//import com.transportke.models.Staff;
import com.transportke.models.WorkLog;

@ManagedBean(name="workLogController", eager=true)
@SessionScoped
public class WorkLogController {
	
	@ManagedProperty(value="#{staffController}")
	private StaffController staffController;
	
	private WorkLog workLog;
	
	@PostConstruct
	public void init() {}
	
	public void refresh() {
		if(staffController.getStaff().getId() > 0) {
		    workLog = WorkLogDAO.findOpenWorkLog(staffController.getStaff());
		} else {
			redirect("staff-login.xhtml");
		}
	}
	
	public String openWorkLog() {
		WorkLogDAO.openWorkLog(staffController.getStaff());
		workLog = new WorkLog();
	    //return "staff.xhtml?faces-redirect=true";
		return null;
	}
	
	public String closeWorkLog() {
	    WorkLogDAO.closeWorkLog(workLog);
	    //return "staff.xhtml?faces-redirect=true";
	    return null;
	}
	 
	public WorkLog getWorkLog() {
		return workLog;
	}

	public void setWorkLog(WorkLog workLog) {
		this.workLog = workLog;
	}

	public StaffController getStaffController() {
		return staffController;
	}

	public void setStaffController(StaffController staffController) {
		this.staffController = staffController;
	}
	
	public void redirect(String url) {
	    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    try {
			externalContext.redirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
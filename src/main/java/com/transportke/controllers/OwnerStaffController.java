package com.transportke.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Sha256Hash;

import com.transportke.dao.OwnerStaffDAO;
import com.transportke.models.OwnerStaff;
import com.transportke.util.Configs;
import com.transportke.util.Emailer;

@ManagedBean(name="ownerStaffController", eager=true)
@SessionScoped
public class OwnerStaffController {
	
	private OwnerStaff ownerStaff, loggedInOwnerStaff;
	private List<OwnerStaff> ownerStaffList;
	private String clearPassword,message;
	private UIComponent component;
	//private SessionManager sessionManager;
	
	
	@PostConstruct
	public void init() {
		ownerStaff = new OwnerStaff();
	}
	
	public OwnerStaffController() {
		ownerStaffList = OwnerStaffDAO.getOwnerStaffList();
	}
	
	public void reset(ActionEvent event){
		ownerStaff = new OwnerStaff();
	}
	
	public String addOwnerStaff() {
		ownerStaff.setCreatedBy("Self");
		ownerStaff.setEditedBy("Self");
		OwnerStaffDAO.addOwnerStaff(ownerStaff);
		ownerStaffList = OwnerStaffDAO.getOwnerStaffList();
		ownerStaff = new OwnerStaff();
	    return "owner.xhtml?faces-redirect=true";
	}
	
	public String updateOwnerStaff() {
	    OwnerStaffDAO.updateOwnerStaff(ownerStaff);
	    ownerStaffList = OwnerStaffDAO.getOwnerStaffList();
	    ownerStaff = new OwnerStaff();
	    return "owner.xhtml?faces-redirect=true";
	}
	
	public OwnerStaff getOwnerStaff() {
		return ownerStaff;
	}

	public void setOwnerStaff(OwnerStaff ownerStaff) {
		this.ownerStaff = ownerStaff;
	}

	public OwnerStaff getLoggedInOwnerStaff() {
		return loggedInOwnerStaff;
	}

	public void setLoggedInOwnerStaff(OwnerStaff loggedInOwnerStaff) {
		this.loggedInOwnerStaff = loggedInOwnerStaff;
	}

	public List<OwnerStaff> getOwnerStaffList() {
		return ownerStaffList;
	}

	public void setOwnerStaffList(List<OwnerStaff> ownerStaffList) {
		this.ownerStaffList = ownerStaffList;
	}
}
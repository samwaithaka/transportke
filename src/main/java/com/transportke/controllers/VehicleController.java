package com.transportke.controllers;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.transportke.dao.SaccoDAO;
import com.transportke.dao.StaffDAO;
import com.transportke.dao.MotorVehicleDAO;
import com.transportke.models.OwnerUser;
import com.transportke.models.MotorVehicle;

@ManagedBean(name="vehicleController", eager=true)
@SessionScoped
public class VehicleController {
	
	private MotorVehicle motorVehicle;
	private OwnerUser ownerUser;
	private List<MotorVehicle> motorVehicleList;
	
	@ManagedProperty(value="#{ownerUserController}")
	private OwnerUserController ownerUserController;
	
	@PostConstruct
	public void init() {
		motorVehicle = new MotorVehicle();
		ownerUser = new OwnerUser();
	}
	
	public VehicleController() {
	}
	
	public void refresh() {
		ownerUser = ownerUserController.getOwnerUser();
		motorVehicleList = MotorVehicleDAO.getOwnerMotorVehicleList(ownerUser);
	}
	public String addVehicle() {
		motorVehicle.setInsuranceExpiryDate(new java.sql.Date(motorVehicle.getInsuranceExpiryDate().getTime()));
		motorVehicle.setLastNTSAInspectionDate(new java.sql.Date(motorVehicle.getLastNTSAInspectionDate().getTime()));
		motorVehicle.setSpeedGovernorAnnualCertExpiry(new java.sql.Date(motorVehicle.getSpeedGovernorAnnualCertExpiry().getTime()));
		motorVehicle.setSacco(SaccoDAO.find(motorVehicle.getSacco().getId()));
		motorVehicle.setDriver(StaffDAO.find(motorVehicle.getDriver().getId()));
		motorVehicle.setConductor(StaffDAO.find(motorVehicle.getConductor().getId()));
		motorVehicle.setCreatedBy("Self");
		motorVehicle.setEditedBy("Self");
		motorVehicle.setOwnerUser(ownerUser);
		MotorVehicleDAO.addMotorVehicle(motorVehicle);
		motorVehicleList = MotorVehicleDAO.getOwnerMotorVehicleList(ownerUser);
		motorVehicle = new MotorVehicle();
	    return "admin-list-motorVehicle.xhtml?faces-redirect=true";
	}
	
	public String updateVehicle() {
		motorVehicle.setInsuranceExpiryDate(new java.sql.Date(motorVehicle.getInsuranceExpiryDate().getTime()));
		motorVehicle.setLastNTSAInspectionDate(new java.sql.Date(motorVehicle.getLastNTSAInspectionDate().getTime()));
		motorVehicle.setSpeedGovernorAnnualCertExpiry(new java.sql.Date(motorVehicle.getSpeedGovernorAnnualCertExpiry().getTime()));
		motorVehicle.setSacco(SaccoDAO.find(motorVehicle.getSacco().getId()));
		motorVehicle.setDriver(StaffDAO.find(motorVehicle.getDriver().getId()));
		motorVehicle.setConductor(StaffDAO.find(motorVehicle.getConductor().getId()));
	    MotorVehicleDAO.updateMotorVehicle(motorVehicle);
	    motorVehicleList = MotorVehicleDAO.getOwnerMotorVehicleList(ownerUser);
	    motorVehicle = new MotorVehicle();
	    return "admin-list-motorVehicle.xhtml?faces-redirect=true";
	}
	 
	public MotorVehicle getMotorVehicle() {
		return motorVehicle;
	}

	public void setMotorVehicle(MotorVehicle motorVehicle) {
		this.motorVehicle = motorVehicle;
	}

	public List<MotorVehicle> getMotorVehicleList() {
		return motorVehicleList;
	}

	public void setMotorVehicleList(List<MotorVehicle> motorVehicleList) {
		this.motorVehicleList = motorVehicleList;
	}

	public OwnerUser getOwnerUser() {
		return ownerUser;
	}

	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}
	
	public OwnerUserController getOwnerUserController() {
		return ownerUserController;
	}

	public void setOwnerUserController(OwnerUserController ownerUserController) {
		this.ownerUserController = ownerUserController;
	}

	public void redirect(String url) {
	    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    try {
			externalContext.redirect(url);
		} catch (IOException e) {e.printStackTrace();}
	}
}
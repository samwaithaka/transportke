package com.transportke.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Sha256Hash;

import com.transportke.dao.OwnerUserDAO;
import com.transportke.dao.StaffDAO;
import com.transportke.dao.WorkLogDAO;
import com.transportke.models.OwnerUser;
import com.transportke.models.Staff;
import com.transportke.util.Configs;
import com.transportke.util.Emailer;

@ManagedBean(name="ownerUserController", eager=true)
@SessionScoped
public class OwnerUserController {
	
	private OwnerUser ownerUser, loggedInOwnerUser;
	private List<OwnerUser> ownerUserList;
	private List<Staff> staffList;
	private String clearPassword,message;
	//private SessionManager sessionManager;
	
	
	@PostConstruct
	public void init() {
		ownerUser = new OwnerUser();
	}
	
	public OwnerUserController() {
		ownerUserList = OwnerUserDAO.getOwnerUserList();
		staffList = StaffDAO.getOwnerStaffList(ownerUser);
	}
	
	public void refresh() {
		if(ownerUser.getId() == 0) {
			redirect("owner-login.xhtml");
		}
	}
	
	public String addOwnerUser() {
		//clearPassword = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(0,8);
		//ownerUser.setPassword(encryptPassword(ownerUser.getPassword()));
		ownerUser.setCreatedBy("Self");
		ownerUser.setEditedBy("Self");
		//user.setResetFlag(true);
		if(OwnerUserDAO.findExistingOwnerUser(ownerUser.getIdNumber()).getId() == 0) {
			OwnerUserDAO.addOwnerUser(ownerUser);
			String from = Configs.getConfig("adminemail");
			String to = ownerUser.getEmailAddress();
			String subject = Configs.getConfig("systemname") + " OwnerUser Account Creation";
			String body = 
					"Dear " + ownerUser.getOwnerName() + ", " +
					"<p>Your user account on has been successfully created. Use the following " +
					"credentials: </p>" +
					"Username: " + ownerUser.getIdNumber() +
					"<br />Password: " + clearPassword +
					"<p>You can access the system from this link: " + Configs.getConfig("appurl") +
					"</p>System Admin";
			Emailer.send(from, to, subject, body);
		} else {
			FacesContext fc = FacesContext.getCurrentInstance();
			FacesMessage msg = new FacesMessage("OwnerUser Exists!","This user is already in the system.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
	    	fc.addMessage(null, msg);
	    	fc.renderResponse();
		}
		ownerUserList = OwnerUserDAO.getOwnerUserList();
		ownerUser = new OwnerUser();
	    return "admin-list-users.xhtml?faces-redirect=true";
	}
	
	public void updateOwnerUser() {
		/*user.setEditedBy(loggedInOwnerUser.getOwnerUsername());
	    OwnerUserDAO.updateOwnerUser(user);
	    ownerUserList = OwnerUserDAO.getOwnerUserList();
	    user = new OwnerUser();
	    return "admin-list-users.xhtml?faces-redirect=true";*/
	}
	
	public void login2() {
       /* ExternalContext eContext = FacesContext.getCurrentInstance().getExternalContext();  
        String url = AuthManager.login(eContext, ownerUser.getOwnerUsername(),ownerUser.getPassword());
        Session session = AuthManager.getSession();
        message = (String) session.getAttribute("message");
        loggedInOwnerUser = (OwnerUser) session.getAttribute("user");
        return url;*/
    }
	public String login() {
		ownerUser = OwnerUserDAO.login(ownerUser.getIdNumber(), ownerUser.getPassword()); 
		return "owner.xhtml";
	}
	public OwnerUser getOwnerUser() {
		return ownerUser;
	}

	public void setOwnerUser(OwnerUser ownerUser) {
		this.ownerUser = ownerUser;
	}

	public OwnerUser getLoggedInOwnerUser() {
		return loggedInOwnerUser;
	}

	public void setLoggedInOwnerUser(OwnerUser loggedInOwnerUser) {
		this.loggedInOwnerUser = loggedInOwnerUser;
	}

	public List<OwnerUser> getOwnerUserList() {
		return ownerUserList;
	}

	public void setOwnerUserList(List<OwnerUser> ownerUserList) {
		this.ownerUserList = ownerUserList;
	}
	
	public String resetPassword() throws UnsupportedEncodingException {
		String newPassword = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(0,8);
		//user.setResetFlag(true);
		//user.setEditedBy(loggedInOwnerUser.getOwnerUsername());
		ownerUser.setPassword(encryptPassword(newPassword));
		//OwnerUserDAO.updateOwnerUserPassword(user);
		
		String from = Configs.getConfig("adminemail");
		String to = ownerUser.getEmailAddress();
		String subject = Configs.getConfig("systemname") + " Password Reset";
		/*String body = "" +
				"Dear " + ownerUser.getFirstName() + ", " +
				"<p>Your password has been reset. Use the following " +
				"credentials, and then change your password as prompted: </p>" +
				"OwnerUsername: " + ownerUser.getOwnerUsername() +
				"<br />Password: " + newPassword +
				"<p>You can access the system from this link: " + Configs.getConfig("appurl") + 
				"</p>System Admin";
		Emailer.send(from, to, subject, body);*/
		
		/*	FacesContext fc = FacesContext.getCurrentInstance();
		FacesMessage msg = new FacesMessage("Reset Successfully","Password has been successfully reset and email sent to " + ownerUser.getFirstName() + " " + ownerUser.getLastName());
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
    	fc.addMessage(null, msg);
    	fc.renderResponse();*/
    	return null;
	}
	
	public String changePassword() throws IOException {
		/*loggedInOwnerUser.setEditedBy(loggedInOwnerUser.getOwnerUsername());
		loggedInOwnerUser.setPassword(encryptPassword(loggedInOwnerUser.getPassword()));
		loggedInOwnerUser.setResetFlag(false);
		OwnerUserDAO.updateOwnerUserPassword(loggedInOwnerUser);*/
		return "logout.xhtml?faces-redirect=true";
	}
	
	private static String encryptPassword(String password) {
		DefaultHashService hashService = new DefaultHashService();
		hashService.setHashIterations(512);
		hashService.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
		//hashService.setPrivateSalt(new SimpleByteSource(PRIVATE_SALT)); // Same salt as in shiro.ini, but NOT base64-encoded.
		hashService.setGeneratePublicSalt(true);
		DefaultPasswordService passwordService = new DefaultPasswordService();
		passwordService.setHashService(hashService);
		String encryptedPassword = passwordService.encryptPassword(password);
		return encryptedPassword;
	}

	public void validatePassword(ComponentSystemEvent event) {
		String PASSWORD_PATTERN = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
		Pattern pattern;
		Matcher matcher;
		
		FacesContext fc = FacesContext.getCurrentInstance();
		UIComponent components = event.getComponent();
		// get password
		UIInput uiInputPassword = (UIInput) components.findComponent("password");
		String password = uiInputPassword.getLocalValue() == null ? ""
				: uiInputPassword.getLocalValue().toString();
		String passwordId = uiInputPassword.getClientId();
		// get confirm password
		UIInput uiInputConfirmPassword = (UIInput) components.findComponent("confirm-password");
		String confirmPassword = uiInputConfirmPassword.getLocalValue() == null ? ""
				: uiInputConfirmPassword.getLocalValue().toString();

		if (password.isEmpty() || confirmPassword.isEmpty()) {
			FacesMessage msg = new FacesMessage("Required!","Enter password and confirm!");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        	fc.addMessage(passwordId, msg);
        	fc.renderResponse();
		    return;
		}
        // Password strengths rules
		pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        if(!matcher.matches()){
        	FacesMessage msg =
        			new FacesMessage("Weak Password!","Make sure that your password has combination of lower and upper case, a number and a special character");
        	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        	fc.addMessage(passwordId, msg);
        	fc.renderResponse();
        } else if (!password.equals(confirmPassword)) {
        	FacesMessage msg = new FacesMessage("Passwords do not match!");
        	msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        	fc.addMessage(passwordId, msg);
        	fc.renderResponse();
        }
	}

	public List<Staff> getStaffList() {
		return staffList;
	}

	public void setStaffList(List<Staff> staffList) {
		this.staffList = staffList;
	}
	public void redirect(String url) {
	    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    try {
			externalContext.redirect(url);
		} catch (IOException e) {e.printStackTrace();}
	}
}
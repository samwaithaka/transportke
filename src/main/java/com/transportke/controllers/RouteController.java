package com.transportke.controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.transportke.dao.RouteDAO;
import com.transportke.models.Route;

@ManagedBean(name="routeController", eager=true)
@SessionScoped
public class RouteController {
	
	private Route route;
	private List<Route> routeList;
	
	@PostConstruct
	public void init() {
		route = new Route();
	}
	
	public RouteController() {
		routeList = RouteDAO.getRouteList();
	}
	
	public String addRoute() {
		route.setCreatedBy("Self");
		route.setEditedBy("Self");
		RouteDAO.addRoute(route);
		routeList = RouteDAO.getRouteList();
		route = new Route();
	    return "admin-list-routes.xhtml?faces-redirect=true";
	}
	
	public String updateRoute() {
		//user.setEditedBy(loggedInRoute.getRoutename());
	    RouteDAO.updateRoute(route);
	    routeList = RouteDAO.getRouteList();
	    route = new Route();
	    return "admin-list-routes.xhtml?faces-redirect=true";
	}
	 
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public List<Route> getRouteList() {
		return routeList;
	}

	public void setRouteList(List<Route> routeList) {
		this.routeList = routeList;
	}
}
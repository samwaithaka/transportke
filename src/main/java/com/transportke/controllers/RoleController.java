package com.transportke.controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.transportke.dao.RoleDAO;
import com.transportke.models.Role;

@ManagedBean(name="roleController", eager=true)
@SessionScoped
public class RoleController {
	
	private Role role;
	private List<Role> roleList;
	
	@PostConstruct
	public void init() {
		role = new Role();
	}
	
	public RoleController() {
		roleList = RoleDAO.getRoleList();
	}
	
	public String addRole() {
		role.setCreatedBy("Self");
		role.setEditedBy("Self");
		RoleDAO.addRole(role);
		roleList = RoleDAO.getRoleList();
		role = new Role();
	    return "admin-list-roles.xhtml?faces-redirect=true";
	}
	
	public String updateRole() {
		//user.setEditedBy(loggedInRole.getRolename());
	    RoleDAO.updateRole(role);
	    roleList = RoleDAO.getRoleList();
	    role = new Role();
	    return "admin-list-roles.xhtml?faces-redirect=true";
	}
	 
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
}
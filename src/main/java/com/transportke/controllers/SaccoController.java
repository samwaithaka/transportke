package com.transportke.controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.transportke.dao.RoleDAO;
import com.transportke.dao.RouteDAO;
import com.transportke.dao.SaccoDAO;
import com.transportke.models.Sacco;

@ManagedBean(name="saccoController", eager=true)
@SessionScoped
public class SaccoController {
	
	private Sacco sacco;
	private List<Sacco> saccoList;
	
	@PostConstruct
	public void init() {
		sacco = new Sacco();
	}
	
	public SaccoController() {
		saccoList = SaccoDAO.getSaccoList();
	}
	
	public String addSacco() {
		sacco.setRoute(RouteDAO.find(sacco.getRoute().getId()));
		sacco.setCreatedBy("Self");
		sacco.setEditedBy("Self");
		SaccoDAO.addSacco(sacco);
		saccoList = SaccoDAO.getSaccoList();
		sacco = new Sacco();
	    return "admin-list-saccos.xhtml?faces-redirect=true";
	}
	
	public String updateSacco() {
		sacco.setRoute(RouteDAO.find(sacco.getRoute().getId()));
		//user.setEditedBy(loggedInSacco.getSacconame());
	    SaccoDAO.updateSacco(sacco);
	    saccoList = SaccoDAO.getSaccoList();
	    sacco = new Sacco();
	    return "admin-list-saccos.xhtml?faces-redirect=true";
	}
	 
	public Sacco getSacco() {
		return sacco;
	}

	public void setSacco(Sacco sacco) {
		this.sacco = sacco;
	}

	public List<Sacco> getSaccoList() {
		return saccoList;
	}

	public void setSaccoList(List<Sacco> saccoList) {
		this.saccoList = saccoList;
	}
}